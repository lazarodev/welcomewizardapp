package com.gt.dev.lazaro.welcomewizardapp.feature.welcome;

import com.gt.dev.lazaro.welcomewizardapp.R;
import com.stephentuso.welcome.BasicPage;
import com.stephentuso.welcome.TitlePage;
import com.stephentuso.welcome.WelcomeActivity;
import com.stephentuso.welcome.WelcomeConfiguration;

/**
 * Created by LazaroDev on 12/12/2017.
 */

public class MyWelcomeActivity extends WelcomeActivity {

    @Override
    protected WelcomeConfiguration configuration() {
        return new WelcomeConfiguration.Builder(this)
                .defaultBackgroundColor(R.color.colorPrimary)
                .page(new TitlePage(R.drawable.polar, getString(R.string.app_name)).background(R.color.colorPrimaryDark))
                .page(new BasicPage(R.drawable.polar, getString(R.string.app_name), getString(R.string.app_name)).background(R.color.colorAccent))
                .swipeToDismiss(true)
                .build();
    }
}
