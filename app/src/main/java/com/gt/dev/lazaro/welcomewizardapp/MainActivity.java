package com.gt.dev.lazaro.welcomewizardapp;

import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.gt.dev.lazaro.welcomewizardapp.feature.welcome.MyWelcomeActivity;
import com.stephentuso.welcome.WelcomeHelper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private WelcomeHelper welcomeHelper;
    private Button btnTest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // WelcomeActivity instance
        welcomeHelper = new WelcomeHelper(this, MyWelcomeActivity.class);
        welcomeHelper.show(savedInstanceState);

        btnTest = findViewById(R.id.btn_main_test);
        btnTest.setOnClickListener(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        welcomeHelper.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_main_test:
                // We force to show the WelcomeActivity Again
                welcomeHelper.forceShow();
                break;
        }
    }
}
